package com.example.microservices.currencyexchangeservice.web;

import com.example.microservices.currencyexchangeservice.modal.ExchangeValue;
import com.example.microservices.currencyexchangeservice.service.IExchangeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyExchangeController {

    @Autowired
    private IExchangeValueService iExchangeValueService;

    @GetMapping("/currency-exchange/from/{from}/to/{to}")
    public ExchangeValue retrieveExchangeValue(@PathVariable String from, @PathVariable String to)
    {
        return iExchangeValueService.findByFromAndTo(from, to);
    }
}
