package com.example.microservices.currencyexchangeservice.service.impl;

import com.example.microservices.currencyexchangeservice.dao.hibernate.ExchangeValueRepository;
import com.example.microservices.currencyexchangeservice.modal.ExchangeValue;
import com.example.microservices.currencyexchangeservice.service.IExchangeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class ExchangeValueService implements IExchangeValueService {

    @Autowired
    private Environment environment;

    @Autowired
    private ExchangeValueRepository exchangeValueRepository;

    @Override
    public ExchangeValue findByFromAndTo(String from, String to) {
        ExchangeValue exchangeValue = exchangeValueRepository.findByFromAndTo(from, to);
        exchangeValue.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
        return exchangeValue;
    }

}
