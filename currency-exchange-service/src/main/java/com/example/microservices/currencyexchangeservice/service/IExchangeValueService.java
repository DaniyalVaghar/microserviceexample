package com.example.microservices.currencyexchangeservice.service;

import com.example.microservices.currencyexchangeservice.modal.ExchangeValue;

public interface IExchangeValueService {
    ExchangeValue findByFromAndTo(String from, String to);
}
